from pathlib import Path
import os

from . import bge_config


def main() -> None:
    config = bge_config._find_bge_config(Path(os.getcwd()))
    print(config)
