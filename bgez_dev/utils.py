from typing import Mapping, Any

__all__ = [
    '_get'
]


def _get(mapping: Mapping, *keys: str, default: Any = None) -> Any:
    '''
    Safely access nested elements in nested mappings.
    '''
    try:
        item = mapping
        for key in keys:
            item = item[key]
        return item
    except KeyError:
        return default
